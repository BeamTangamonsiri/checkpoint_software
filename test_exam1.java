package checkpoint;

import static org.junit.Assert.*;

import java.util.Collection;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runners.Parameterized.Parameters;

public class test_exam1 {
	
	public int n, result;
	
	@Parameters
	public void Collection() {
	}
	
	public test_exam1(int n, int result) {
		this.n = n;
		this.result = result;
	}

	@Test
	public void test() {
		Assert.assertFalse(exam1.exam(0));
		Assert.assertFalse(exam1.exam(1));
		Assert.assertFalse(exam1.exam(4));
		Assert.assertTrue(exam1.exam(5));
//		System.out.println(exam1.exam(5));
	}

}
